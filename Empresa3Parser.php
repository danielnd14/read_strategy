<?php

require_once("LineParser.php");

class Empresa3Parser extends BaseParser
{

    public function __construct()
    {
        parent::__construct("/(?s)(?<=empresa3\>.).*?(?=\<fim)/");
    }

    public function parseLineToReg($line): ?Registro
    {
        $values = preg_split("/,/", $line);

        if (!isset($values) || count($values) != 5) {
            return NULL;
        }

        try {
            return new Registro(new DateTimeImmutable($values[0]),
                @$values[3], @$values[2], @$values[1], @$values[4]);
        } catch (Exception $ignored) {
            return null;
        }
    }
}

?>