<?php

class Registro
{
    private DateTimeImmutable $dateTime;
    private float $nivelPrincipal;
    private float $nivelSecundario;
    private float $desnivelCentral;
    private float $perda;

    public function __construct(DateTimeImmutable $dateTime, float $nivelPrincipal, float $nivelSecundario, float $desnivelCentral, float $perda)
    {
        $this->dateTime = $dateTime;
        $this->nivelPrincipal = $nivelPrincipal;
        $this->nivelSecundario = $nivelSecundario;
        $this->desnivelCentral = $desnivelCentral;
        $this->perda = $perda;
    }

    public function getDateTime(): DateTimeImmutable
    {
        return $this->dateTime;
    }

    public function getNivelPrincipal(): float
    {
        return $this->nivelPrincipal;
    }

    public function getNivelSecundario(): float
    {
        return $this->nivelSecundario;
    }

    public function getDesnivelCentral(): float
    {
        return $this->desnivelCentral;
    }

    public function getPerda(): float
    {
        return $this->perda;
    }

    public function __toString(): string
    {
        return json_encode(array(
            "dateTime" => $this->getDateTime()->format("c"),
            "nivelPrincipal" => $this->getNivelPrincipal(),
            "nivelSecundario" => $this->getNivelSecundario(),
            "desnivelCentral" => $this->getDesnivelCentral(),
            "perda" => $this->getPerda()
        ));
    }

}

?>