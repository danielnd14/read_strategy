<?php

abstract class LineParser
{

    public function parseAllLines($lines): array
    {
        $regs = [];
        foreach ($lines as $line) {
            $reg = $this->parseLineToReg($line);
            if (isset($reg))
                $regs[] = $reg;
        }
        return $regs;
    }

    public abstract function parseLineToReg($line): ?Registro;
}

?>