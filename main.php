<?php
require("Empresa1Parser.php");
require("Empresa2Parser.php");
require("Empresa3Parser.php");

$fileContent = file_get_contents("FileContent.txt");

$strategies = array(new Empresa1Parser(),
    new Empresa2Parser(),
    new Empresa3Parser());

$allRegsFromFile = [];//Array de array, cada posição tem todos os regs de uma empresa

foreach ($strategies as $strategy) {
    $allRegsFromFile[] = $strategy->parseFullContent($fileContent);
}
unset($strategy);

var_dump($allRegsFromFile);//mostra todos os registros lidos

?>