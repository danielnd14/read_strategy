<?php
require_once("LineParser.php");

abstract class BaseParser extends LineParser
{
    private $myPattern;

    public function __construct(string $myPattern)
    {
        $this->myPattern = $myPattern;
    }

    public function parseFullContent(string $fullContent): array
    {
        preg_match_all($this->myPattern, $fullContent, $matches);
        $myContent = $matches[0][0];//fullmatch
        $lines = preg_split("/\n/", $myContent);
        return parent::parseAllLines($lines);
    }


}

?>